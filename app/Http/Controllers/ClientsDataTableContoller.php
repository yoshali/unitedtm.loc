<?php

namespace App\Http\Controllers;

use App\DataTables\ClientsDataTable;
use App\Models\Client;
use Illuminate\Http\Request;

class ClientsDataTableContoller extends Controller
{
    public function index(ClientsDataTable $dataTable)
    {
        return $dataTable->render('clients');
    }

    public function update(Request $request)
    {
        $data = $request->input();
        if(!empty($data['data'])) {
            foreach($data['data'] as $key => $value) {
                $client = Client::find($key);
                $client->update($value);
            }
            return response()->json($data);
        }
    }
}
