<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        // Create 10000 product records
        for ($i = 0; $i < 10000; $i++) {
            Client::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'patronymic' => $faker->firstNameMale,
                'job' => $faker->jobTitle,
                'age' => $faker->numberBetween($min = 12, $max = 74),
            ]);
        }
    }
}
