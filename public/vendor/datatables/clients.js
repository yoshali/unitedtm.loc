$(function () {
    window.LaravelDataTables = window.LaravelDataTables || {};
    window.LaravelDataTables["clients-table"] = $("#clients-table").DataTable({
        "serverSide": true,
        "processing": true,
        "ajax": {
            "url": "http:\/\/unitedtm.loc\/clients", "type": "GET", "data": function (data) {
                for (var i = 0, len = data.columns.length; i < len; i++) {
                    if (!data.columns[i].search.value) delete data.columns[i].search;
                    if (data.columns[i].searchable === true) delete data.columns[i].searchable;
                    if (data.columns[i].orderable === true) delete data.columns[i].orderable;
                    if (data.columns[i].data === data.columns[i].name) delete data.columns[i].name;
                }
                delete data.search.regex;
            }
        },
        "columns": [{
            "data": "action",
            "name": "action",
            "title": "Action",
            "orderable": false,
            "searchable": false,
            "width": 60,
            "className": "text-center"
        }, {"data": "id", "name": "id", "title": "Id", "orderable": true, "searchable": true}, {
            "data": "first_name",
            "name": "first_name",
            "title": "First Name",
            "orderable": true,
            "searchable": true
        }, {
            "data": "created_at",
            "name": "created_at",
            "title": "Created At",
            "orderable": true,
            "searchable": true
        }, {"data": "updated_at", "name": "updated_at", "title": "Updated At", "orderable": true, "searchable": true}],
        "dom": "Bfrtip",
        "order": [[1, "desc"]],
        "buttons": [{"extend": "create"}, {"extend": "export"}, {"extend": "print"}, {"extend": "reset"}, {"extend": "reload"}]
    });
})
;
