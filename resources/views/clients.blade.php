<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Test app</title>
    <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/datatables/editor.dataTables.css') }}">


    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script>
    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.editor.js') }}"></script>

    <style type="text/css">
        .table-content {
            margin: 50px;
        }
    </style>
</head>
<body>
    <div class="table-content">
        {{$dataTable->table()}}
    </div>

    <script type="text/javascript">
        var editor;
        $(function () {

            editor = new $.fn.dataTable.Editor( {
                ajax: "{{route('clients')}}",
                table: "#clients-table",
                idSrc:  'id',
                fields: [
                    {
                        label: "First name",
                        name: "first_name"
                    },
                    {
                        label: "Last name",
                        name: "last_name"
                    },
                    {
                        label: "Patronymic",
                        name: "patronymic"
                    },
                    {
                        label: "Job",
                        name: "job"
                    },
                    {
                        label: "Age",
                        name: "age"
                    },
                ]
            } );

            $('#clients-table').on( 'click', 'tbody td:not(:first-child)', function (e) {
                editor.inline( this );
            } );

            window.LaravelDataTables = window.LaravelDataTables || {};
            window.LaravelDataTables["clients-table"] = $("#clients-table").DataTable({
                "serverSide": true,
                "processing": true,
                "ajax": {
                    "url": "{{ route('clients') }}", "type": "GET", "data": function (data) {
                        delete data.search.regex;
                    }
                },
                "columns": [
                    {
                        "data": "id",
                        "name": "id",
                        "title": "Id",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "first_name",
                        "name": "first_name",
                        "title": "First Name",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "last_name",
                        "name": "last_name",
                        "title": "Last Name",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "patronymic",
                        "name": "patronymic",
                        "title": "Patronymic",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "job",
                        "name": "job",
                        "title": "Job",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "age",
                        "name": "age",
                        "title": "Age",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "created_at",
                        "name": "created_at",
                        "title": "Created At",
                        "orderable": true,
                        "searchable": true
                    },
                    {
                        "data": "updated_at",
                        "name": "updated_at",
                        "title": "Updated At",
                        "orderable": true,
                        "searchable": true
                    }
                ],
                "dom": "Bfrtip",
                "order": [[1, "desc"]],
                "pageLength" : 17,
            });
        });
    </script>
</body>
</html>
