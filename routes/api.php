<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('clients', 'App\Http\Controllers\ClientsController@index');

Route::get('clients/{client}', 'App\Http\Controllers\ClientsController@show');

//В задании не было маршрута на создание, но пусть полежит
/*Route::post('clients','ClientsController@store');*/

Route::put('clients/{client}','App\Http\Controllers\ClientsController@update');

Route::delete('clients/{client}', 'App\Http\Controllers\ClientsController@delete');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
