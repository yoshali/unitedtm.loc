<?php

use Illuminate\Support\Facades\Route;
use App\Models\Client;
use App\Http\Controllers\ClientsDataTableContoller;

Route::get('/', function (){
    return redirect('clients');
});

Route::get('/clients', [ ClientsDataTableContoller::class, 'index'])->name('clients');
Route::post('/clients', [ ClientsDataTableContoller::class, 'update'])->name('clients')->middleware(['auth']);

/*Route::get('meow', function (){
    return view('meow');
});*/
/*Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');*/

require __DIR__.'/auth.php';
